$(document).ready(function() {
    mouseOver();
});

const mouseOver = () => {
    $(".social").hover(
        function(e) {
            $(function () {
                $("#button-label").html(e.target.id);
            });
        },
        function() {
            $("#button-label").html("");
            
        }
    );
}

let gitHubRepos = new XMLHttpRequest();
gitHubRepos.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        let gitObject = JSON.parse(this.responseText);
        $("#gitRepos").html("<strong>" + gitObject[0].full_name + ": " + "</strong>" + "<a href=\"" + gitObject[0].svn_url + "\">" + gitObject[0].svn_url + "</a>");
        
    }
};
gitHubRepos.open("GET", "https://api.github.com/users/izaxone/repos", true);
gitHubRepos.send();

